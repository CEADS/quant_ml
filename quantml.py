def find_centers_and_borders(clusters, theoretical_centers, verbose=1):
	if(verbose==2):
		print('Head of clusters:')
		clusters.head()
		print('Head of theoretical centers:')
		theoretical_centers.head()
	clusters = data.groupby("ID")
	clusters_in_array = []
	for _, cluster in clusters:
		cluster_without_id = cluster.drop("ID", axis=1)
		clusters_in_array.append(cluster_without_id.values)
	cluster_ID = 0
	point_distance_per_cluster = []
	for cluster in clusters_in_array:
		cluster_center = centers.loc[centers["ID"] == cluster_ID].values
		index = 0
		distance_per_point = []
		for point in cluster:
			squared_distance = 0
			for x in range(len(point)):
				squared_distance = squared_distance + math.pow(
					(point[x] - cluster_center[index][x]), 2
				)
			distance_per_point.append(math.sqrt(squared_distance))
		index = index + 1
		cluster_ID = cluster_ID + 1
		point_distance_per_cluster.append(distance_per_point)
	coords_nearest_center = []
	coords_on_borders = []
	index = 0
	for cluster in point_distance_per_cluster:
		center_index = cluster.index(min(cluster))
		border_index = cluster.index(max(cluster))
		coords_nearest_center.append(clusters_in_array[index][center_index])
		coords_on_borders.append(clusters_in_array[index][border_index])
		index = index + 1
	centers = pd.DataFrame(coords_nearest_center)
	borders = pd.DataFrame(coords_on_borders)
	if(verbose==2):
		print('Head of points closest to theoretical centers:')
		centers.head()
		print('Head of border points:')
		borders.head()
	return centers, borders
	#Note: not yet tested

def graph1D:
	#TODO
	
def graph2D:
	#TODO
	
def graph3D:
	#TODO
	
def transfer_learn(data_to_transfer_learn_on_x_train, data_to_transfer_learn_on_x_valid, data_to_transfer_learn_on_x_test, data_to_transfer_learn_on_y_train, data_to_transfer_learn_on_y_valid, data_to_transfer_learn_on_y_test, model, num_hidden, max_neurons, dec_neuron_by_this_amount_per_layer, num_epochs, patience):
	model.layers.pop()

	for layer in model.layers[:-1]:
		layer.trainable = False
		
	last = keras.layers.Dense(max_neurons,activation='relu')(model)
	for x in range(num_hidden):
		if((max_neurons - (dec_neuron_by_this_amount_per_layer * x)) > 0):
			last = keras.layers.Dense((max_neurons - (dec_neuron_by_this_amount_per_layer * x)),activation='relu')(last)
		else:
			last = keras.layers.Dense(1,activation='relu')(last)
			
	new_model = keras.models.Model(inputs=model, outputs=last)
	
	new_model.layers.pop()
	
	final_model = keras.models.Sequential([
		new_model,
		keras.layers.Dense(1, activation='relu')
	])
	
	final_model.compile(loss='mean_squared_error', optimizer='adam')
	
	history = final_model.fit(data_to_transfer_learn_on_x_train, data_to_transfer_learn_on_y_train, epochs=num_epochs, validation_data=(data_to_transfer_learn_on_x_valid, data_to_transfer_learn_on_y_valid), callbacks=[keras.callbacks.EarlyStopping(patience=patience)])
	
	evaluation = final_model.evaluate(data_to_transfer_learn_on_x_test, data_to_transfer_learn_on_y_test)
	
	return final_model, evaluation, history
	# Note: not yet tested

	
def make_dense_model(input_shape, padding_size = 50, hidden = 3, loss = 'mse', n_neurons = 50, output_hidden_layers = 1, input_wide_layers = 1, activation = "selu", num_outputs = 1):
	input_layer_shape = [int((input_shape)/input_wide_layers)]
	model = keras.models.Sequential()
	inputs = []
	outputs = []
	final_outputs = []
	for x in range(input_wide_layers):
		inputs.append(keras.Input(shape=input_layer_shape))
	for x in range(len(inputs)):
		last = keras.layers.Dense(n_neurons,activation=activation)(inputs[x])
		for y in range(hidden):
			last = keras.layers.Dense(n_neurons,activation=activation)(last)
		outputs.append(last)
	i = 0
	while len(final_outputs) < num_outputs:
		final_outputs.append(outputs[i])
		i += 1
	for x in range(len(outputs)-1):
		final_outputs[0] = keras.layers.Concatenate()([final_outputs[0],outputs[x+1]])
	for x in range(len(final_outputs)):
		extra = keras.layers.Dense(50,activation=activation)(final_outputs[x])
		for num in range(output_hidden_layers-1):
			extra = keras.layers.Dense(50,activation=activation)(extra)
		final_outputs[x] = keras.layers.Dense(1,activation=activation)(extra)
	model = keras.models.Model(inputs=inputs, outputs=[final_outputs])
	model.compile(loss = loss, optimizer=keras.optimizers.Nadam(clipnorm=1.0))
	return model
	# Note: not yet tested